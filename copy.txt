Let's make a game!
    name:Villager
    by:Espresso
    desc:Forge a Village with your bare hands. Learn to craft weapons and tools to creat a town, and attract new villagers, who will help you to build a mighty empire.
    created:09/10/2019
    updated:09/11/2019
    version:0.0.1

Settings
    background:https://pipe.miroware.io/5d802e15b72a043564fd781e/bigtreeforest.png
    building cost increase:115%
    building cost refund:50%
    spritesheet:infrastructureicons, 48 by 48, https://pipe.miroware.io/5d4f2e27927ab206ecfaefef/cloud-idler/infrastructureiconsprite.png
    spritesheet:resourceicons, 48 by 48, https://pipe.miroware.io/5d4f2e27927ab206ecfaefef/cloud-idler/resourceiconsprite.png
    spritesheet:upgradeicons, 48 by 48, https://pipe.miroware.io/5d4f2e27927ab206ecfaefef/cloud-idler/upgradesiconsprite.png
    stylesheet:https://gitlab.com/aroffler/villager/raw/master/css.txt


Buttons
    *treeButton
        name:punch wood
        desc:Click the Tree to punch it and get logs.
        on click:yield 1 log
        //on click:if (have goldenHacks and chance(1%)) yield 1 goldenbit
        //TODO: needs better name for goldenClick
        icon:https://pipe.miroware.io/5d802e15b72a043564fd781e/pixil-frame-0(1).png
        no text
        class:bigButton
        icon class:shadowed
        tooltip origin:bottom
        tooltip class:red

    *stoneButton
        name:Mine stone
        desc:Click the Stone to Mine and get Stone
        on click:yield 1 stone
        icon:https://pipe.miroware.io/5d802e15b72a043564fd781e/stonequarry-pixilart(1).png
        no text
        class:bigButton
        icon class:shadowed
        tooltip origin:bottom
        tooltip class:red

Layout
    *main
        contains:res, buttons
    *res
        contains:Resources
        class:fullWidth
    *buttons
        contains:Buttons
    *buildings
        contains:BulkDisplay, Buildings
        header:Infrastructure
        class:infrastructure
        tooltip origin:right
    *upgrades
        contains:Upgrades
        header:Upgrades
        class:upgrades
        tooltip origin:top
        tooltip class:tooltip-upgrades


Resources
    *log|logs
        name:log|logs
        desc:wood is essential for your early survival, build huts, tools, and other nessacary items to survive!
        icon:resourceicons[0,0]
        class:noBackground
        show earned

    *stone|stones
        name:stone|stones
        desc:stone is an evolutionary step in your survival, build better tools and forge weapons to smite the monsters lurking about!
        icon:resourceicons[1,0]
        class:noBackground
        show earned


Buildings
//Infrastructure
    *TEMPLATE
        on click:anim glow

    //Wood lane
    *sharpstick|sharpsticks
        name:Sharp Stick|Sharp Sticks
        desc:A Sharp Stick is better than bloody fists.
        icon:infrastructureicons[1,0]
        cost:250 logs
        on tick:yield 10000 log
        class:infrastructure-button
        unlocked

    *flintrock|flintrocks
        name:Flint Rock|Flint Rocks
        desc:A Peice of Flint rock found in a nearby stream allows you to gather wood faster and more efficiently.
        icon:infrastructureicons[0,0]
        cost:500 logs
        on tick:yield 5 logs
        class:infrastructure-button
        req:500 logs:earned



    //Stone lane

    //Iron lane



Upgrades
    *TEMPLATE
        on click:anim glow

    //DataCenter Upgrades
    *firehardening
        name:FireHardening
        desc:You discover the ability to use fire and harden the point of your sharp stick, making it much easier to use.<//><b>Effect:</b><.>Sharpstick use multiplied x.2
        icon:upgradeicons[0,0]
        no text
        cost:1000 logs
        passive:multiply yield of sharpstick by .2
        class:upgrades-button
        req:(sharpstick>=25)

    *sinewwrap
        name:Sinew Wraps
        desc:While dresing up some animal you have managed to kill, you realize the sinews or tendons are strong and supple <i>These would make an exellent protective wrap for your sharpend stick</i><//><b>Effect:</b><.>Sharpstick use multiplied x.5
        icon:upgradeicons[0,1]
        no text
        cost:5000 logs
        passive:multiply yield of sharpstick by .5
        class:upgrades-button
        req:(sharpstick>=50 and have firehardening)


    //CloudCreator upgrades
